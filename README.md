# Projet de Data-mining (Hippolyte Lévêque & Louis Cassedanne)

Introduction
============

Ce projet a pour objectif l'extraction de connaissance de la base de données publique du Grand Débat mis en place par le gouvernement actuel.
Toutes les données peuvent être récupérées à cette adresse : 
	https://granddebat.fr/pages/donnees-ouvertes

Nous avons choisi de traiter le dataset correspondant au thème "Démocratie et Citoyenneté".

Afin d'explorer le travail effectué pour ce projet, il suffit de suivre les étapes suivantes indiquées dans ce README.

Installation du code
====================

Ce projet comprend un Makefile qui vous permet d'installer très simplement tout le code écrit dans `data_mining/` comme une nouvelle librairie python sur votre ordinateur, ainsi que toutes ses dépendances. Pour cela, lancer simplement::

    $ make install


Utilisation des notebooks
=========================

Une fois le projet installé, vous pouvez tester note 
Vous trouverez dans `notebook` les 2 jupyter notebooks qui sont directement exécutables et vous guiderons en parallèle. Nous vous recommandons de commencer avec `notebook/analysis.ipynb`, puis `notebook/elasticsearch.ipynb`


Où se trouve le code?
=====================

Si vous souhaitez évaluer en détail tout le code écrit pour ce projet, celui-ci est disponible dans le répertoire `data_mining`, et est également appelé par deux scripts dans le répertoire `scripts`
