clean:
	@rm -f */version.txt
	@rm -f .coverage
	@rm -fr */__pycache__
	@rm -fr __pycache__
	@rm -fr build
	@rm -fr dist
	@rm -fr data_mining-*.dist-info
	@rm -fr data_mining.egg-info

wheel: clean
	@pip3 install wheel
	@python3 setup.py bdist_wheel

install: clean wheel
	@pip3 install -U dist/*.whl

check_code:
	@flake8 scripts/* data_mining/*.py tests/*.py

