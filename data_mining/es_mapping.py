from os import environ
from elasticsearch import Elasticsearch

# address = "http://localhost:9200"
address = "https://search-deepsense-bqi3aiefnflkilgfsb46uuscum.eu-west-3.es.amazonaws.com/"
env_var = environ.get("ELASTICSEARCH_URL", address)
cnx = Elasticsearch(env_var, timeout=600)


def get_mapping():
    """Defines mapping for different index"""
    return {
        "mappings": {
            "doc": {
                "properties": {
                    "category": {"type": "keyword"},
                    "contrib_id": {"type": "keyword"},
                    "contrib_ref": {"type": "keyword"},
                    "contrib_title": {"type": "keyword"},
                    "contrib_zip_code": {"type": "float"},
                    "date": {"type": "date"},
                    "author_id": {"type": "keyword"},
                    "author_type": {"type": "text"},
                    "author_zip_code": {"type": "float"},
                    "question_id": {"type": "keyword"},
                    "question_type": {"type": "keyword"},
                    "question_answer": {"type": "text"},
                    "question_answer_lem": {"type": "text"},
                    "entities": {
                        "properties": {
                            "CARDINAL_ent": {"type": "keyword"},
                            "DATE_ent": {"type": "keyword"},
                            "EVENT_ent": {"type": "keyword"},
                            "FAC_ent": {"type": "keyword"},
                            "GPE_ent": {"type": "keyword"},
                            "LAW_ent": {"type": "keyword"},
                            "LOC_ent": {"type": "keyword"},
                            "MONEY_ent": {"type": "keyword"},
                            "NORP_ent": {"type": "keyword"},
                            "ORDINAL_ent": {"type": "keyword"},
                            "ORG_ent": {"type": "keyword"},
                            "PERCENT_ent": {"type": "keyword"},
                            "PERSON_ent": {"type": "keyword"},
                            "PRODUCT_ent": {"type": "keyword"},
                            "QUANTITY_ent": {"type": "keyword"},
                            "TIME_ent": {"type": "keyword"},
                            "WORK_OF_ART_ent": {"type": "keyword"},
                        }
                    },
                    'toxic': {"type": "float"},
                    'severe_toxic': {"type": "float"},
                    'obscene': {"type": "float"},
                    'threat': {"type": "float"},
                    'insult': {"type": "float"},
                    'identity_hate': {"type": "float"},
                    'toxicity_indicator': {"type": "float"},
                }
            }
        }
    }
