# -*- coding: UTF-8 -*-
""" Specific lib for data_mining toxicity model
"""

# Import from standard Library
import joblib
import math
import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
import re, string

# Import from data_mining Library

label_cols = ['toxic', 'severe_toxic', 'obscene',
              'threat', 'insult', 'identity_hate']


def read_files():
    train = pd.read_csv('data_mining/data/toxic_comments/train.csv')
    test = pd.read_csv('data_mining/data/toxic_comments/test.csv')
    return train, test


def display_dataset_infos(train):
    print("\n")
    msg = "DATASET DESCRIPTION"
    print(msg)
    print("=" * len(msg))
    train['none'] = 1-train[label_cols].max(axis=1)
    print(train.describe())
    print("\n")


def preprocess_data(train, test):
    train['comment_text'].fillna('unknown', inplace=True)
    test['comment_text'].fillna('unknown', inplace=True)
    return train, test


def tokenize(s):
    re_tok = re.compile(f'([{string.punctuation}“”¨«»®´·º½¾¿¡§£₤‘’])')
    return re_tok.sub(r' \1 ', s).split()


def pr(y_i, y, x):
    p = x[y == y_i].sum(0)
    return (p+1) / ((y == y_i).sum()+1)


def get_mdl(y, x):
    """
    Fit a model for one dependent at a time
    """
    y = y.values
    r = np.log(pr(1, y, x) / pr(0, y, x))
    m = LogisticRegression(C=4, dual=True)
    x_nb = x.multiply(r)
    return m.fit(x_nb, y), r


vec = TfidfVectorizer(ngram_range=(1, 2), tokenizer=tokenize,
                      min_df=3, max_df=0.9, strip_accents='unicode',
                      use_idf=1, smooth_idf=1, sublinear_tf=1)


def build_model():
    train, test = read_files()
    # display_dataset_infos(train)
    train, test = preprocess_data(train, test)
    # TF-IDF instead of binarized features
    n = train.shape[0]
    trn_term_doc = vec.fit_transform(train["comment_text"])
    test_term_doc = vec.transform(test["comment_text"])
    x = trn_term_doc
    # test_x = test_term_doc
    # preds = np.zeros((len(test), len(label_cols)))
    models = {}
    for i, j in enumerate(label_cols):
        print('fit', j)
        models[j] = get_mdl(train[j], x)
    joblib.dump(models, 'toxicity_model')
    joblib.dump(vec, 'vectorizer')
    return vec, models


def predict_toxicity(df, models, vectorizer):
    """
    requires a dataframe with 'content' column and 'id' column at least
    """
    df_x = vectorizer.transform(df['content'])
    preds = np.zeros((len(df), len(label_cols)))
    for i, j in enumerate(label_cols):
        print('    * Predicting', j)
        m,r = models[j]
        preds[:,i] = m.predict_proba(df_x.multiply(r))[:,1]
    final_df = pd.concat([df, pd.DataFrame(preds, columns = label_cols)], axis=1)
    return final_df


if __name__ == "__main__":
    print("Trying to find model :", end=" ", flush=True)
    try:
        models = joblib.load('toxicity_model')
        print("Model found ;", end=" ", flush=True)
        try:
            vectorizer = joblib.load('vectorizer')
            print("Vectorizer found")
        except FileNotFoundError:
            print(" Vectorizer not found => Building model from scratch")
            vectorizer, models = build_model()
    except FileNotFoundError:
        print(" Toxicity model not found => Building model from scratch")
        vectorizer, models = build_model()
