""" Ce script a pour vocation de nettoyer les données et de procéder à la
lemmatization / extraction d'entités pour les réponses "ouvertes". Il est
organisé de la manière suivante :

- Pour chacune des quatre collections (traitant des quatre thèmes du GDN):
    - tri des questions ouvertes vs fermées
    - suppression des contributions des auteurs "tricheurs":
        - un même autheur pour deux contributions
        - mêmes réponses au mot près pour les questions ouvertes
- Lemmatization des réponses ouvertes et extraction des entités détectées
"""


import os
import pandas as pd
import json
import pickle
import spacy
""" Partie I : chargement des données et suppression des contributions
illicites"""
# Lecture des 4 fichiers csv et mise en dataframe
data = "../raw_data/"
subdirs = [subdir for subdir in os.listdir(data)]
dict_df = {}
for subdir in subdirs:
    path = data + subdir
    dict_df[subdir[:2]] = pd.read_csv(path)

df_OE = dict_df["OE"]
questions_OE = list(df_OE.columns[11:])
questions_title_OE = [question[20:] for question in questions_OE]
questions_id_OE = [question[:17] for question in questions_OE]
df_OE.columns=list(df_OE.columns[:11]) + questions_id_OE
id_to_title_OE = {}
title_to_id_OE = {}
# Les questions sont identifiées grâce à leur ID
for i in range(len(questions_id_OE)):
    id_to_title_OE[questions_id_OE[i]] = questions_title_OE[i]
    title_to_id_OE[questions_title_OE[i]]= questions_id_OE[i]

# Tri questions ouvertes / fermées
questions_ouvertes_OE=[]
questions_ouvertes_OE.append(questions_title_OE[0])
questions_ouvertes_OE.append(questions_title_OE[2])
questions_ouvertes_OE.append(questions_title_OE[4])
questions_ouvertes_OE.append(questions_title_OE[5])
questions_ouvertes_OE.append(questions_title_OE[8])
questions_ouvertes_OE.append(questions_title_OE[9])
questions_ouvertes_OE.append(questions_title_OE[10])
questions_ouvertes_OE.append(questions_title_OE[13])
questions_ouvertes_OE.append(questions_title_OE[14])
questions_ouvertes_OE.append(questions_title_OE[16])
questions_ouvertes_OE.append(questions_title_OE[18])
questions_ouvertes_OE.append(questions_title_OE[19])
questions_ouvertes_OE += questions_title_OE[20:33]
questions_fermees_OE = [el for el in questions_title_OE if el not in questions_ouvertes_OE]
questions_ouvertes_id_OE = [title_to_id_OE[el] for el in questions_ouvertes_OE]
questions_fermees_id_OE = [title_to_id_OE[el] for el in questions_fermees_OE]
df_fermees_OE = df_OE[questions_fermees_id_OE]
df_ouvertes_OE = df_OE[questions_ouvertes_id_OE]
# Identification des auteurs ayant contribué plusieurs fois
counter_OE = df_OE.groupby('authorId').count()
correct_ids_OE = counter_OE.loc[counter_OE["id"]==1,:]
correct_ids_OE = list(correct_ids_OE.index)
cheating_ids_OE = counter_OE.loc[counter_OE["id"]!=1,:]
cheating_ids_OE = list(cheating_ids_OE.index)
cheating_ids = []
cheating_ids+= cheating_ids_OE
correct_df_OE = df_OE.loc[df_OE["authorId"].isin(correct_ids_OE), :]
# Identification des auteurs ayant dupliqué des réponses aux questions ouvertes
duplicates_OE = correct_df_OE.duplicated(subset=questions_ouvertes_id_OE)
cheating_indexes_OE = []
for index,value in enumerate(duplicates_OE):
    if value:
        cheating_indexes_OE.append(index)
new_cheating_ids_OE = [correct_df_OE.iloc[el]["authorId"] for el in cheating_indexes_OE]
# Mise à jour d'une liste d'auteurs tricheurs
cheating_ids += new_cheating_ids_OE
cheating_ids = list(set(cheating_ids))

"""Même traitement pour les trois autres jeux de données"""


df_DC = dict_df["DC"]
questions_DC = list(df_DC.columns[11:])
questions_title_DC = [question[20:] for question in questions_DC]
questions_id_DC = [question[:17] for question in questions_DC]
df_DC.columns=list(df_DC.columns[:11]) + questions_id_DC
id_to_title_DC = {}
title_to_id_DC = {}
for i in range(len(questions_id_DC)):
    id_to_title_DC[questions_id_DC[i]] = questions_title_DC[i]
    title_to_id_DC[questions_title_DC[i]]= questions_id_DC[i]
questions_ouvertes_DC = []
questions_ouvertes_DC.append(questions_title_DC[0])
questions_ouvertes_DC.append(questions_title_DC[2])
questions_ouvertes_DC.append(questions_title_DC[3])
questions_ouvertes_DC.append(questions_title_DC[5])
questions_ouvertes_DC.append(questions_title_DC[6])
questions_ouvertes_DC.append(questions_title_DC[8])
questions_ouvertes_DC.append(questions_title_DC[9])
questions_ouvertes_DC.append(questions_title_DC[11])
questions_ouvertes_DC.append(questions_title_DC[12])
questions_ouvertes_DC.append(questions_title_DC[14])
questions_ouvertes_DC.append(questions_title_DC[15])
questions_ouvertes_DC.append(questions_title_DC[16])
questions_ouvertes_DC.append(questions_title_DC[18])
questions_ouvertes_DC.append(questions_title_DC[19])
questions_ouvertes_DC.append(questions_title_DC[20])
questions_ouvertes_DC.append(questions_title_DC[21])
questions_ouvertes_DC.append(questions_title_DC[22])
questions_ouvertes_DC.append(questions_title_DC[23])
questions_ouvertes_DC.append(questions_title_DC[24])
questions_ouvertes_DC.append(questions_title_DC[25])
questions_ouvertes_DC.append(questions_title_DC[26])
questions_ouvertes_DC.append(questions_title_DC[27])
questions_ouvertes_DC.append(questions_title_DC[28])
questions_ouvertes_DC.append(questions_title_DC[29])
questions_ouvertes_DC.append(questions_title_DC[31])
questions_ouvertes_DC.append(questions_title_DC[32])
questions_ouvertes_DC.append(questions_title_DC[33])
questions_ouvertes_DC.append(questions_title_DC[34])
questions_ouvertes_DC.append(questions_title_DC[35])
questions_ouvertes_DC.append(questions_title_DC[36])
questions_fermees_DC = [q for q in questions_title_DC if q not in questions_ouvertes_DC]
questions_ouvertes_id_DC = [title_to_id_DC[el] for el in questions_ouvertes_DC]
questions_fermees_id_DC = [title_to_id_DC[el] for el in questions_fermees_DC]
counter_DC = df_DC.groupby('authorId').count()
correct_ids_DC = counter_DC.loc[counter_DC["id"]==1,:]
correct_ids_DC = list(correct_ids_DC.index)
cheating_ids_DC = counter_DC.loc[counter_DC["id"]!=1,:]
cheating_ids_DC = list(cheating_ids_DC.index)
cheating_ids += cheating_ids_DC
cheating_ids = list(set(cheating_ids))
correct_df_DC = df_DC.loc[df_DC["authorId"].isin(correct_ids_DC), :]
correct_ouvertes_DC = correct_df_DC[["authorId"]+questions_ouvertes_id_DC]
duplicates_DC = correct_df_DC.duplicated(subset=questions_ouvertes_id_DC)
cheating_indexes_DC = []
for index,value in enumerate(duplicates_DC):
    if value:
        cheating_indexes_DC.append(index)
new_cheating_ids_DC = [correct_df_DC.iloc[el]["authorId"] for el in cheating_indexes_DC]
cheating_ids += new_cheating_ids_DC
cheating_ids = list(set(cheating_ids))


df_FI = dict_df["FI"]
questions_FI = list(df_FI.columns[11:])
questions_title_FI = [question[20:] for question in questions_FI]
questions_id_FI = [question[:17] for question in questions_FI]
df_FI.columns=list(df_FI.columns[:11]) + questions_id_FI
id_to_title_FI = {}
title_to_id_FI = {}

for i in range(len(questions_id_FI)):
    id_to_title_FI[questions_id_FI[i]] = questions_title_FI[i]
    title_to_id_FI[questions_title_FI[i]]= questions_id_FI[i]
questions_ouvertes_FI = []
questions_ouvertes_FI.append(questions_title_FI[0])
questions_ouvertes_FI.append(questions_title_FI[1])
questions_ouvertes_FI.append(questions_title_FI[2])
questions_ouvertes_FI.append(questions_title_FI[4])
questions_ouvertes_FI.append(questions_title_FI[5])
questions_ouvertes_FI.append(questions_title_FI[6])
questions_ouvertes_FI.append(questions_title_FI[7])
questions_fermees_FI = [questions_title_FI[3]]
questions_ouvertes_id_FI = [title_to_id_FI[el] for el in questions_ouvertes_FI]
questions_fermees_id_FI = [title_to_id_FI[el] for el in questions_fermees_FI]
counter_FI = df_FI.groupby('authorId').count()
correct_ids_FI = counter_FI.loc[counter_FI["id"]==1,:]
correct_ids_FI = list(correct_ids_FI.index)
cheating_ids_FI = counter_FI.loc[counter_FI["id"]!=1,:]
cheating_ids_FI = list(cheating_ids_FI.index)
cheating_ids += cheating_ids_FI
cheating_ids = list(set(cheating_ids))
correct_df_FI = df_FI.loc[df_FI["authorId"].isin(correct_ids_FI), :]
duplicates_FI = correct_df_FI.duplicated(subset=questions_ouvertes_id_FI)
cheating_indexes_FI = []
for index,value in enumerate(duplicates_FI):
    if value:
        cheating_indexes_FI.append(index)
new_cheating_ids_FI = [correct_df_FI.iloc[el]["authorId"] for el in cheating_indexes_FI]
cheating_ids += new_cheating_ids_FI
cheating_ids = list(set(cheating_ids))




df_TE = dict_df["TE"]
questions_TE = list(df_TE.columns[11:])
questions_title_TE = [question[20:] for question in questions_TE]
questions_id_TE = [question[:17] for question in questions_TE]
df_TE.columns=list(df_TE.columns[:11]) + questions_id_TE
id_to_title_TE = {}
title_to_id_TE = {}

for i in range(len(questions_id_TE)):
    id_to_title_TE[questions_id_TE[i]] = questions_title_TE[i]
    title_to_id_TE[questions_title_TE[i]]= questions_id_TE[i]
questions_ouvertes_TE = []
questions_ouvertes_TE.append(questions_title_TE[0])
questions_ouvertes_TE.append(questions_title_TE[1])
questions_ouvertes_TE.append(questions_title_TE[3])
questions_ouvertes_TE.append(questions_title_TE[5])
questions_ouvertes_TE.append(questions_title_TE[6])
questions_ouvertes_TE.append(questions_title_TE[7])
questions_ouvertes_TE.append(questions_title_TE[9])
questions_ouvertes_TE.append(questions_title_TE[11])
questions_ouvertes_TE.append(questions_title_TE[12])
questions_ouvertes_TE.append(questions_title_TE[13])
questions_ouvertes_TE.append(questions_title_TE[14])
questions_ouvertes_TE.append(questions_title_TE[15])

questions_fermees_TE = [q for q in questions_title_TE if q not in questions_ouvertes_TE]
questions_ouvertes_id_TE = [title_to_id_TE[el] for el in questions_ouvertes_TE]
questions_fermees_id_TE = [title_to_id_TE[el] for el in questions_fermees_TE]

counter_TE = df_TE.groupby('authorId').count()
correct_ids_TE = counter_TE.loc[counter_TE["id"]==1,:]
correct_ids_TE = list(correct_ids_TE.index)
cheating_ids_TE = counter_TE.loc[counter_TE["id"]!=1,:]
cheating_ids_TE = list(cheating_ids_TE.index)
cheating_ids += cheating_ids_TE
cheating_ids = list(set(cheating_ids))


correct_df_TE = df_TE.loc[df_FI["authorId"].isin(correct_ids_TE), :]
correct_ouvertes_TE = correct_df_TE[["authorId"]+questions_ouvertes_id_TE]


duplicates_TE = correct_df_TE.duplicated(subset=questions_ouvertes_id_TE)
cheating_indexes_TE = []
for index,value in enumerate(duplicates_TE):
    if value:
        cheating_indexes_TE.append(index)
new_cheating_ids_TE = [correct_df_TE.iloc[el]["authorId"] for el in cheating_indexes_TE]

cheating_ids += new_cheating_ids_TE
cheating_ids = list(set(cheating_ids))


# On enregistre la liste des auteurs frauduleux
with open('../cheating_ids.pickle','wb') as f:
    pickle.dump(cheating_ids,f)



""" Partie II, lemmatization, extraction des entités"""


def lemmatize_strict(doc):
    """ lemmatize token"""
    list_lemma = []
    for token in doc:
        cond1 = not token.is_punct
        cond2 = not token.is_space
        cond3 = token.is_alpha
        cond4 = not token.lemma_ == '-PRON-'
        cond5 = not token.like_num
        cond6 = not token.is_digit
        cond7 = len(token) > 1
        cond8 = not token.is_stop
        if (cond1 and cond2 and cond3 and cond4
                and cond5 and cond6 and cond7 and cond8):
            list_lemma.append(token.lemma_)
    return ' '.join(list_lemma)

def get_all_entities(doc):
    """
    Should return a dict
    d = {"loc_ent": ["Parc du Château", ...],
         "org_ent": ["ONU", ...],
         ...}
    """
    ent_list = ['LOC', 'PERSON', 'NORP', 'FAC', 'ORG', 'GPE', 'PRODUCT',
                'EVENT', 'WORK_OF_ART', 'LAW', 'LANGAGE', 'DATE',
                'TIME', 'PERCENT', 'MONEY', 'QUANTITY', 'CARDINAL', 'ORDINAL']
    d = {}
    for ent in ent_list:
        d['{}_ent'.format(ent)] = ent_recognizer(doc, ent)
    return d

def ent_recognizer(doc, ent_name):
    ents = [(ent.string, ent.label_) for ent in doc.ents
            if ent.label_ == ent_name]
    return ents

def process_response(response, nlp):
    answer = {}
    body_nlp = nlp(response)
    answer["lemma"] = lemmatize_strict(body_nlp)
    # Lemmatization
    #Named Entities Recognition
    answer["entities"] = get_all_entities(body_nlp)
    return answer


# Lemmatization et extraction d'entités & reformatage des données en json
dfs = {"TE":df_TE,"FI":df_FI,"OE":df_OE,"DC":df_DC}
correct_dict = {}
for key in dfs:
    correct_dict[key] = dfs[key].to_dict()
questions_ouvertes_dict = {"TE":questions_ouvertes_id_TE,"FI":questions_ouvertes_id_FI,"OE":questions_ouvertes_id_OE,"DC":questions_ouvertes_id_DC}
questions_fermees_dict = {"TE":questions_fermees_id_TE,"FI":questions_fermees_id_FI,"OE":questions_fermees_id_OE,"DC":questions_fermees_id_DC}
corrected_dic = {}
for key in dfs.keys():
    questions_ouvertes=questions_ouvertes_dict[key]
    questions_fermees=questions_fermees_dict[key]
    dico = correct_dict[key]
    json_file = []
    for i in range(len(dico["id"])):
        dic = {}
        for el in dico.keys():
            if el in questions_ouvertes:
                dic[el] = {}
                dic[el]["content"] = dico[el][i]
                dic[el]["type"] = "ouverte"
            elif el in questions_fermees:
                dic[el] = {}
                dic[el]["content"] = dico[el][i]
                dic[el]["type"] = "fermee"
            else:
                dic[el] = dico[el][i]
        try:
            dic["ZipCode"] = int(dico["authorZipCode"][i])
        except:
            dic["ZipCode"] = None
        dic["cat"] = key
        json_file.append(dic)
    corrected_dic[key] = json_file
nlp = spacy.load('fr_core_news_md')
for cat in corrected_dic.keys():
    json_file = corrected_dic[cat]
    for el in json_file:
        for key in list(el.keys())[11:-2]:
            if el[key]["type"] == "ouverte":
                if pd.notna(el[key]["content"]):
                    processed_content = process_response(el[key]["content"],nlp)
                    el[key]["lemmatized_content"] = processed_content["lemma"]
                    el[key]["entities"] = processed_content["entities"]
    with open('FI.json',"w") as f:
        json.dump(json_file,f)