# !/usr/bin/env python3
# -*- coding: UTF-8 -*-

# Import from Standard Library
from elasticsearch import helpers
import json
from json.decoder import JSONDecodeError
import pandas as pd
import joblib
import pickle
from data_mining.toxicity import predict_toxicity, build_model


def inject_data(cnx, data, index='grand_debat'):
    """
    inject data in ES. Data is a list of dict where
    each dict is a document
    """
    helpers.bulk(cnx, data, index=index, doc_type="doc")
    cnx.indices.flush(index)


def inject_csv_file(cnx, path, index='grand_debat'):
    # Load author ids to remove
    print("Loading author ids to be removed", end=" ", flush=True)
    with open("cheating_ids.pickle", "rb") as f:
        cheating_ids = pickle.load(f)
    print("=> SUCCESS")
    # Read and process
    chunksize = 50000
    df_iter = pd.read_csv(path, chunksize=chunksize)
    for df in df_iter:
        print(" NEW ITERATION")
        df = df.dropna()
        try:
            df = df.drop(['Unnamed: 0'], axis=1)
        except KeyError:
            pass
        df = df.reset_index()
        df['date'] = df['date'].str[:10]
        data = df.to_dict(orient='records')
        print('   * Updating documents ...', end=" ", flush=True)
        ent_errors = 0
        true_data = []
        for i in range(len(data)):
            try:
                data[i]['entities'] = json.loads(data[i]['entities'].replace("'", '"'))
            except JSONDecodeError:
                del data[i]['entities']
                ent_errors += 1
            if data[i]['author_id'] not in cheating_ids:
                true_data.append(data[i])
        print("SUCCESS")
        # Inject
        print("    * Injecting {} docs :".format(len(true_data)), end=" ", flush=True)
        inject_data(cnx, true_data, index)
        print("    SUCCESS, entities errors = {}\n".format(ent_errors))
