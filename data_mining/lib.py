# -*- coding: UTF-8 -*-
""" Main lib for data_mining project
"""

# Import from standard Library
import sys
from datetime import datetime

def duration(start, now=None, linehead=''):
    """
    Prettify duration in days, hours, mins and sec
    start = datetime(2015, 10, 11, 1, 46, 31, 151236)
    end   = datetime(2015, 10, 11, 3, 1, 33, 151236)
    assert duration(start, end) == '1 hours 15 min 2 sec'
    start = datetime(2015, 10, 11, 1, 46, 31, 151236)
    end   = datetime(2015, 10, 12, 3, 1, 33, 151236)
    assert duration(start, end) == '1 days 1 hours 15 min 2 sec'
    start = datetime(2015, 10, 11, 1, 46, 31, 151236)
    end   = datetime(2015, 10, 12, 23, 46, 33, 151236)
    assert duration(start, end) == '1 days 22 hours 0 min 2 sec'
    start = datetime(2015, 10, 11, 1, 46, 31, 151236)
    end   = datetime(2015, 10, 11, 1, 55, 13, 151236)
    assert duration(start, end) == '8 min 42 sec'
    start = datetime(2015, 10, 11, 1, 46, 31, 151236)
    end   = datetime(2015, 10, 11, 1, 47, 17, 151236)
    assert duration(start, end) == '0 min 46.0 sec'
    start = datetime(2015, 10, 11, 1, 46, 31, 151236)
    end   = datetime(2015, 10, 11, 1, 47, 17, 131036)
    """
    if not now:
        now = datetime.now()
    delta = now - start
    days, seconds = delta.days, delta.total_seconds()
    hours = int(seconds // 3600 - 24 * days)
    minutes = int((seconds % 3600) // 60)

    seconds = round(seconds % 60, 2)

    if (days, hours, minutes) != (0, 0, 0):
        seconds = round(seconds, 2)
    dateformat = '{} min {} sec'.format(minutes, seconds)
    if hours:
        dateformat = '{} hours '.format(hours) + dateformat
    if days:
        dateformat = '{} days '.format(days) + dateformat
    return linehead + dateformat


def reformate_data(data):
    new_data = []
    i = 0
    for doc in data:
        questions = {key: doc[key] for key in doc if key[:2] == 'QU'}
        for q in questions:
            new_doc = {}
            new_doc['category'] = doc['cat']
            new_doc['contrib_id'] = doc['id']
            new_doc['contrib_ref'] = doc['reference']
            new_doc['contrib_title'] = doc['title']
            new_doc['contrib_zip_code'] = doc['ZipCode']
            new_doc['date'] = doc['createdAt']
            new_doc['author_id'] = doc['authorId']
            new_doc['author_type'] = doc['authorType']
            new_doc['author_zip_code'] = doc['authorZipCode']
            new_doc['question_id'] = q
            try:
                new_doc['question_type'] = questions[q]['type']
                new_doc['question_answer'] = questions[q]['content']
                try:
                    new_doc['question_answer_lem'] = questions[q]['lemmatized_content']
                    new_doc['entities'] = questions[q]['entities']
                except KeyError:
                    pass
            except TypeError:
                pass
            new_data.append(new_doc)
            i += 1
    return new_data




def query_yes_no(question, default="yes"):
    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")
    return True
