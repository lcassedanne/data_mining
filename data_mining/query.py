# !/usr/bin/env python
# -*- coding: UTF-8 -*-

# Import from Standard Library

# Import from data_mining and others
from data_mining.es_mapping import cnx, get_mapping
from data_mining.lib import query_yes_no


def get_count(cnx, index="grand_debat"):
    res = cnx.count(index)
    return res['count']


def get_author_contrib():
    pass


def delete_index(cnx, index='grand_debat'):
    """ delete an index"""
    try:
        cnx.indices.delete(index, ignore=400)
        print('{} index deleted !'.format(index))
    except Exception:
        print('{} not found!'.format(index))


def create_index(cnx, index='grand_debat', force=False):
    """Creates and set mapping for a given index"""
    if force:
        mapping = get_mapping()
        if cnx.indices.exists(index):
            delete_index(cnx, index)
        cnx.indices.create(index=index, body=mapping)
        print('==> "{}" index successfully reset !'.format(index))
    else:
        question = 'Do you really want to reset "{}" index?'
        q = query_yes_no(question.format(index))
        if q:
            mapping = get_mapping()
            if cnx.indices.exists(index):
                delete_index(cnx, index)
            cnx.indices.create(index=index, body=mapping)
            print('==> "{}" index successfully reset !'.format(index))
        else:
            print('==> "{}" index not reset.')


if __name__ == "__main__":
    create_index(cnx)
