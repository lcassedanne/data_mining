# !/usr/bin/env python3
# -*- coding: UTF-8 -*-

# Import from Standard Library
from py_translator import Translator


def fr_to_en(text):
    s = Translator().translate(text=text, dest='en').text
    return s
