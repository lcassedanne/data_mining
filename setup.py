#!/usr/bin/env python3

from setuptools import find_packages
from setuptools import setup

# PyPI dependencies should be put here
with open("./requirements.pip", "r") as f:
    requirements = f.read()

dependency_links = [
    "https://github.com/scrapy/scrapy@master --no-dependencies --upgrade",
    "https://github.com/explosion/spacy-models/releases/download/en_core_web_sm-2.0.0/en_core_web_sm-2.0.0.tar.gz",
]

setup(
    name='data_mining',
    setup_requires=['setuptools_scm'],
    use_scm_version=True,
    description="Skeleton for data science & machine learning projects",
    packages=find_packages(),
    test_suite='tests',
    install_requires=requirements,
    dependency_links=dependency_links,
    # include_package_data: to install data from MANIFEST.in
    include_package_data=True,
    scripts=["scripts/dm_init_index", 
             "scripts/dm_inject"],
    zip_safe=False,
)

